import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(user: string, pwd: string): boolean{
    console.log(user, pwd);
    if(user === 'user' && pwd === 'password'){
      console.log('logueado...');
      localStorage.setItem('username', user);
      return true;
    }
    console.log('No logueado...');
    return false;
  }

  logout(){
    localStorage.removeItem('username');
    console.log('Salistes de la aplicación');
  }

  getUser(): any{
    return localStorage.getItem('username');
  }

  isLoggued(): boolean{
    console.log(this.getUser() !== null && this.getUser() !== null ? 'Estas Logueado.' : 'No estás logueado.');
    return this.getUser() !== null && this.getUser() !== null;
  }
}
