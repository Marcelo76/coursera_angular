import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje';
import { AppState, APP_CONFIG, AppConfig, myDataBase } from '../app.module';
import { Store } from '@ngrx/store';
import { NuevoDestinoAccion, ElegidoFavoritoAction, EliminarDestinoAction, DestinoViajesState } from '../models/destinos-viajes-states';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { db } from '../app.module';

@Injectable({
  providedIn: 'root'
})
export class DestinosApiClientService {

  constructor(
    private store: Store<AppState>,
    private http: HttpClient,
    @Inject(forwardRef(()=> APP_CONFIG)) private config: AppConfig
  ){}

  add(d: DestinoViaje){
    //this.store.dispatch(new NuevoDestinoAccion(d));
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', {nuevo: d}, {headers: headers} );
    this.http.request(req).subscribe((data: HttpResponse<{}>) => {
      if(data.status === 200){
        this.store.dispatch(new NuevoDestinoAccion(d));

        //Implementando Dexie
        const myDb = db;  //db está declarada y exportada desde app.module.ts
        myDb.destinos.add(d);
        console.log('Todos los destinos de la DB');
        myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }

  elegir(d: DestinoViaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

  eliminar(d: DestinoViaje){
    this.store.dispatch(new EliminarDestinoAction(d));
  }

  get(id: string){
    return this.store.select(state => state.destinos.items.filter(i => i.id === id));
  }

  /*
  public destinos: DestinoViaje[] = [];
  current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null); //Definiendo el observable que almacena el destino actualmente elegido por el usuario

  constructor() { }

  add(d: DestinoViaje){
    d.id = Math.floor((Math.random() * 100) + 1);
    this.destinos.push(d);
  }

  getAll(): DestinoViaje[]{
    return this.destinos;
  }

  elegir(d: DestinoViaje){
    this.destinos.map(x => x.setSelected(x === d));
    this.current.next(d); //Setea cómo elemento actualmente elegido al elemento "d" recibido por parámetro;
  }

  getById(id: number):DestinoViaje{
    return this.destinos.find(d => d.id === id);
  }

  subscribeOnChange(fn: any){
    return this.current.subscribe(fn);
  }
  */
}
