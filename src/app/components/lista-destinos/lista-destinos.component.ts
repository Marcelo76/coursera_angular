import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje';
import { DestinosApiClientService } from 'src/app/services/destinos-api-client.service';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClientService]
})

export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;  //Recibe el evento desde el componente hijo desde "form-destino-viajes.component.ts"
  @Output() onItemDeleted: EventEmitter<DestinoViaje>;  //Recibe el evento desde el componente hijo desde "form-destino-viajes.component.ts"
  public titulo: string = 'Lista de destinos';
  public updates: string[] = [];
  all;

  constructor(
      public _destinosApiClientService: DestinosApiClientService,
      private store: Store<AppState>
    ) {
    this.onItemAdded = new EventEmitter();
    this.onItemDeleted = new EventEmitter();

    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if(d !== null) {this.updates.push(`Se ha elegido a ${d.nombre}`);}
    })
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

  agregaItem(destino: DestinoViaje){
    this._destinosApiClientService.add(destino);
    this.onItemAdded.emit(destino);

    //Limpia los cuadros de texto
    let txtNombre = <HTMLInputElement>document.getElementById('nombre');
    let txtUrl = <HTMLInputElement>document.getElementById('imagenUrl');
    txtNombre.value = '';
    txtUrl.value = '';

    return false;
  }

  public seleccionarItem(e: DestinoViaje){
    this._destinosApiClientService.elegir(e);
  }

  public eliminarItem(d: DestinoViaje){
    if(window.confirm("¿Desea eliminar el destino?")){
      this._destinosApiClientService.eliminar(d);
      this.onItemDeleted.emit(d);
    }
    return false;
  }s
}
