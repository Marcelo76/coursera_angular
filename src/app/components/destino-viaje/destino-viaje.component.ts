import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje';
import { Store } from '@ngrx/store';
import { VoteUpAction, VoteDownAction } from 'src/app/models/destinos-viajes-states';
import { AppState } from '../../app.module';
import { VoteResetAction } from '../../models/destinos-viajes-states';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
    trigger('esFavorito',[
      state('estadoFavorito',style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNFavorito',style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ]
  )]
})

export class DestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input() id: number = 0;
  @HostBinding('attr.class') cssClass="col-md-4";
  @Output() clickItem: EventEmitter<DestinoViaje>;
  @Output() clickDeleteItem: EventEmitter<DestinoViaje> = new EventEmitter();

  constructor(private store: Store<AppState>) {
    this.clickItem = new EventEmitter();
   }

  ngOnInit() {
    console.log(this.destino.getServicios());
  }

  //Se expone el evento para ser desencadenado (con un click) desde otro componente,
  //en este caso desde lista-destinos.component, ya que el botón al que se le da el click
  //se encuentra en éste componente, pero el evento es manejado en el otro componente
  public seleccionar(){
    this.clickItem.emit(this.destino);
  }

  public eliminar(){
    this.clickDeleteItem.emit(this.destino);
  }


  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  resetVotes(){
    this.store.dispatch(new VoteResetAction(this.destino));
    return false;
  }
}
