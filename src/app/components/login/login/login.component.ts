import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public messageErrror: string = '';

  constructor(public _autService: AuthService) { }

  ngOnInit() {
  }

  logIn(user: string, pwd: string): boolean{
    this.messageErrror = '';
    if(!this._autService.login(user, pwd)){
      this.messageErrror = 'Usuario y/o contraseña incorrectos.';
      setTimeout(function(){
        this.messageErrror = ''
      }.bind(this), 3500);
    }
    return false;
  }

  logOut(){
    this._autService.logout();
    return false;
  }

}
