import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje';
import { ActivatedRoute, Routes } from '@angular/router';
import { DestinosApiClientService } from '../../services/destinos-api-client.service';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';



//-------------------------------------------------------------------------
/*
export class DestinosApiClientViejo{
  get(id: string){
    console.log('Este es DestinosApiClientViejo');
    return null;
  }
}

interface AppConfig {
  apiEndPoint: string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'mi_api.com'
}

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export class DestinosApiClientDecorated extends DestinosApiClientService{

  constructor(@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
    super(store);
  }

  get(id: string): Observable<DestinoViaje[]>{
    console.log('Llamado por la clase decorada');
    console.log('config :' + this.config.apiEndPoint);
    return super.get(id);
  }
}
*/
//-------------------------------------------------------------------------




@Component({
  selector: 'app-detalle-destino',
  templateUrl: './detalle-destino.component.html',
  styleUrls: ['./detalle-destino.component.css'],
  //providers: [DestinosApiClientService] //Esta línea ya no es necesaria para inyectar servicios, estos ya no necesitan ser registrados cómo providers, almenos en algular 8 o superior
  /*providers:[
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    {provide: DestinosApiClientViejo, useClass: DestinosApiClientDecorated},
    {provide: DestinosApiClientDecorated, useClass: DestinosApiClientService }
  ]*/
})
export class DetalleDestinoComponent implements OnInit {
  public destino: DestinoViaje;
  style = {
    sources:{
      world:{
        type:'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers:[{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(
    private route: ActivatedRoute,
    private _destinosApiClientService: DestinosApiClientService //Utiliza DestinosApiClientDecorated atravéz de DestinosApiClientService
  ) {
    let id: string = this.route.snapshot.paramMap.get('id');
    this._destinosApiClientService.get(id).subscribe(
      (d: any)=> {
        this.destino = d[0];
      }
    );
   }

  ngOnInit() {
  }

}
