import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { DestinoViaje } from '../../models/destino-viaje';
import { DestinosApiClientService } from '../../services/destinos-api-client.service';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from '../../app.module';

@Component({
  selector: 'app-form-destino-viajes',
  templateUrl: './form-destino-viajes.component.html',
  styleUrls: ['./form-destino-viajes.component.css']
})
export class FormDestinoViajesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje> = new EventEmitter();
  public form: FormGroup;
  public largoMaximoNombre: number = 15;
  public searchResult: string[] = [];

  constructor(
    private fb: FormBuilder,
    @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig //Obs.: forwardRef(() => APP_CONFIG) evita un error de referencia circular ya que éste archivo importa elementos desde app.module.ts y app.module.ts referencia a éste archivo y asi sucesivamente, para frenar que esa llamada sea indefina, es que se utiliza la función forwardRef en la inyección.
    ) {

      //Configurando el formGroup y las validaciones
      this.form = fb.group({
      nombre: ['',Validators.compose([
                Validators.required,
                this.nombreValidator,
                this.nombreLargoValidator(this.largoMaximoNombre)
                ])
              ],
      url: ['',[Validators.required, Validators.maxLength(50)]]
      }

    );

    this.form.valueChanges.subscribe((form: any)=>{
      console.log(form);
    });
  }

  ngOnInit() {
    console.log(this.config.apiEndPoint);

    let input = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(input, 'input').
      pipe(
      map((ke: KeyboardEvent)=>(ke.target as HTMLInputElement).value),
      filter(text => text.length > 2),  //Deja pasar a la siguiente acción, sólo aquellos textos superiores a 2 carácteres
      debounceTime(200), //Genera una espera de 200 milisegundos
      distinctUntilChanged(), //Evita duplicidad de datos
      //  switchMap(() => ajax('/assets/datos.json')) //Simula el estar consultando a un webservice
      //).subscribe(ajaxResponse => {
      //  console.log(ajaxResponse);
      //  this.searchResult = ajaxResponse.response
      //}
      switchMap((text: string) => ajax(this.config.apiEndPoint + '/ciudades?q=' + text))
    ).subscribe(
      ajaxResponse => {
          this.searchResult = ajaxResponse.response;
          console.log('filtro :',this.searchResult);
        }
    );

    //Crear el contenido del array de string this.searchResult a un array de strings a un archiuvo de pruebas en la carpetas assets
  }

  agregar(nombre: string, url: string): boolean{
    let destino = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(destino);
    return false;
  }

  nombreValidator(control: FormControl):{[s: string]: boolean}{
    const l = control.value.toString().trim().length;
    if(l>0 && l<5){
      return { minLengthNombre: true };
    }
    return null;
  }

  nombreLargoValidator(maxLong: number):ValidatorFn{
    return (control: FormControl) : {[s: string]: Boolean} | null =>{
      const l = control.value.toString().trim().length;
      if(l>maxLong){
        return { maxLengthNombre: true };
      }
      return null;

    }
  }
}
