import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { DetalleDestinoComponent } from './components/detalle-destino/detalle-destino.component';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajesComponent } from './components/form-destino-viajes/form-destino-viajes.component';
import {
        DestinoViajesState,
        reducerDestinoViajes,
        initializeDestinoViajesState,
        DestinoViajeEffects,
        InitMyDataAction
      } from './models/destinos-viajes-states';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import Dexie, { DbEvents } from 'dexie';  //npm install dexie --save
import { DestinoViaje } from './models/destino-viaje';
import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { map, flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './directivas/espiame.directive';
import { TrackingClickDirective } from './directivas/tracking-click.directive';

//Las rutas se encuentran en "app-routing.module.ts" (En las versiones antiguas de Angular las rutas se configuraban en éste módulo)

//app config
export interface AppConfig {
  apiEndPoint: string;
}

const APP_CONFIG_VALUE: AppConfig  = {
  apiEndPoint: 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
//Fin app config

// app_init
export function init_app(appLoadService: AppLoadService) : () =>Promise<any>{
  return () => appLoadService.initializeDestinoViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient){}

  async initializeDestinoViajesState():Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// Fin app_init

//Implementación REDUX
export interface AppState {
  destinos: DestinoViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinoViajes
}

const reducerInitialState = {
  destinos: initializeDestinoViajesState()
}
//Fin implementación REDUX


//Dexie
export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
    providedIn: 'root'
})

export class myDataBase extends Dexie{
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>

  constructor(){
    super('myDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value '
    });
  }
}

export const db = new myDataBase();
//Fin Dexie


//i18n ini
class TranslationLoader implements TranslateLoader {

  constructor(private http: HttpClient){}

  getTranslation(lang: string): Observable<any>{
    const promise = db.translations
                    .where('lang')
                    .equals(lang)
                    .toArray()
                    .then(results => {
                        if(results.length === 0){
                          return this.http
                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
                            .toPromise()
                            .then(apiResult => {
                              db.translations.bulkAdd(apiResult);
                              return apiResult;
                            });
                        }
                        return results;
                    }).then((traducciones) => {
                      console.log('traducciones cargandas');
                      console.log(traducciones);
                      return traducciones;
                    }).then((traducciones) => {
                      return traducciones.map(t => ({[t.key]: t.value}));
                    });
        return from(promise).pipe(flatMap(elems => from(elems)));
  }
}


function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}
//i18n end

@NgModule({
  declarations: [
    AppComponent,
    ListaDestinosComponent,
    DestinoViajeComponent,
    DetalleDestinoComponent,
    FormDestinoViajesComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackingClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, {
        initialState: reducerInitialState,
        runtimeChecks:{
          strictStateImmutability: false, //Agregar éstas líneas para permitir mutar al objeto contenido en Redux (No es la idea)
          strictActionImmutability: false //Agregar éstas líneas para permitir mutar al objeto contenido en Redux (No es la idea)
        }
      }),
    EffectsModule.forRoot([DestinoViajeEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService, { provide: APP_INITIALIZER, useFactory: init_app, deps: [ AppLoadService], multi: true},
    myDataBase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
