import { NgModule, Component, ViewChildren } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DetalleDestinoComponent } from './components/detalle-destino/detalle-destino.component';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: "full"},
  { path: 'home', component: ListaDestinosComponent },
  { path: 'lista-destino', component: ListaDestinosComponent },
  { path: 'detalle-destino/:id', component: DetalleDestinoComponent},
  { path: 'login', component: LoginComponent },
  { path: 'protected', component: ProtectedComponent, canActivate: [ UsuarioLogueadoGuard]},
  { path: 'vuelos', component: VuelosComponent, canActivate: [ UsuarioLogueadoGuard ],
    children: [
      { path: '', redirectTo: 'main', pathMatch: 'full'},
      { path: 'main', component: VuelosMainComponent },
      { path: 'mas-info', component: VuelosMasInfoComponent },
      { path: ':id', component: VuelosDetalleComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
