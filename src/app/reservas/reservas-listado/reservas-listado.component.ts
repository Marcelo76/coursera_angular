import { Component, OnInit } from '@angular/core';
import { ReservsApiClientService } from '../reservs-api-client.service';

@Component({
  selector: 'app-reservas-listado',
  templateUrl: './reservas-listado.component.html',
  styleUrls: ['./reservas-listado.component.css']
})
export class ReservasListadoComponent implements OnInit {

  constructor(public _api: ReservsApiClientService) { }

  ngOnInit() {
  }

}
