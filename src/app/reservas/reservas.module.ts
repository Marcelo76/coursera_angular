import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReservasRoutingModule } from './reservas-routing.module';
import { ReservasListadoComponent } from './reservas-listado/reservas-listado.component';
import { ReservasDetalleComponent } from './reservas-detalle/reservas-detalle.component';
import { ReservsApiClientService } from './reservs-api-client.service';


@NgModule({
  declarations: [ReservasListadoComponent, ReservasDetalleComponent],
  imports: [
    CommonModule,
    ReservasRoutingModule
  ],
  //providers:[ReservsApiClientService] //Ya no es necesario registrar los services como proveedores para poder inyectarlos, al almenos en angular 8 o superior
})
export class ReservasModule { }

//ng g m reservas/reservas --module app --flat --routing
