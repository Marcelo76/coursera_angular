import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { TrackingClickAction } from '../models/destinos-viajes-states';

@Directive({
  selector: '[appTrackingClick]'
})
export class TrackingClickDirective {
  private element: HTMLInputElement;

  constructor(private elRef: ElementRef, private store: Store<AppState>) {  //Se recibe, a travéz de inyección de dependecias, el elemento sobre el cual se está aplicando ésta directiva
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(e => this.track(e)); //Nos subcribimos al click del elemento sobre el cual se está implementando la directiva
   }

   track(e: Event){
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags').value.split(' ');
    const idDestino = this.element.attributes.getNamedItem('id').value;
    this.store.dispatch(new TrackingClickAction(idDestino))
    console.log(`|||||||||||| track evento: "${elemTags}"`);

   }

}
