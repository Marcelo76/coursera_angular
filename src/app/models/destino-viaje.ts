import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    selected: boolean = false;
    servicios: string[] = ['piscina','desayuno','Gift card para el casino'];
    id = uuid();
    public clicksCount: number = 0;

    constructor(public nombre: string, public url: string, public votes: number = 0){}

    isSelected(): boolean{
        return this.selected;
    }

    setSelected(sel: boolean){
        this.selected = sel;
    }

    getServicios(){
        return this.servicios;
    }

    votesUp(){
      this.votes++;
    }

    votesDown(){
      this.votes--;
    }

    votesReset(){
      this.votes = 0;
    }

    clicks(){
      this.clicksCount++;
    }

    getClickCount(){
      return this.clicksCount;
    }
}
