import { reducerDestinoViajes, ElegidoFavoritoAction, EliminarDestinoAction, VoteUpAction, VoteDownAction } from './destinos-viajes-states';
import { DestinoViajesState } from './destinos-viajes-states';
import { initializeDestinoViajesState } from './destinos-viajes-states';
import { InitMyDataAction } from './destinos-viajes-states';
import { NuevoDestinoAccion } from './destinos-viajes-states';
import { DestinoViaje } from './destino-viaje';

describe('reducerDestinoViajes', () =>{

  it('should reduce init data', () => {
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1','destino 2']);
    //Action
    const newState: DestinoViajesState = reducerDestinoViajes(prevState, action);
    //Assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
    //Tear down
  });

  it('should reduce new item added data', () => {
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['barcelona','url']);
    //Action
    const newState: DestinoViajesState = reducerDestinoViajes(prevState, action);
    //Assertions
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('barcelona');
    //Tear down
  });


  it('should reduce add new destino', () =>{
      //Setup
      const prevState: DestinoViajesState = initializeDestinoViajesState();
      const destino: DestinoViaje = new DestinoViaje('Talca', 'localhost:300/url_de_prueba', 0);
      const action: NuevoDestinoAccion = new NuevoDestinoAccion(destino);
      //Action
      const newState: DestinoViajesState = reducerDestinoViajes(prevState, action)
      expect(newState.items.length).toEqual(1);
      expect(newState.items[0].nombre).toEqual('Talca');
      //Tear down
  });


  it('should reduce selected favorite', () =>{
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['Paris','destino 2']);
    const destino: DestinoViaje = new DestinoViaje('Paris', 'localhost:300/url_de_prueba', 0);
    const favorite: ElegidoFavoritoAction = new ElegidoFavoritoAction(destino);
    //Action
    const newState1: DestinoViajesState = reducerDestinoViajes(prevState, action);
    const newState2: DestinoViajesState = reducerDestinoViajes(newState1, favorite);
    expect(newState2.items.length).toEqual(2);
    expect(newState2.items[0].nombre).toEqual('Paris');
    //Tear down
  });


  it('should reduce delete destino', () =>{
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const item: DestinoViaje = new DestinoViaje('Paris', 'localhost:300/url_de_prueba', 0);
    const newItem: NuevoDestinoAccion = new NuevoDestinoAccion(item);
    const removeItem: EliminarDestinoAction = new EliminarDestinoAction(item);
    //Action
    const newState1: DestinoViajesState = reducerDestinoViajes(prevState, newItem);
    const newState2: DestinoViajesState = reducerDestinoViajes(newState1, removeItem);
    expect(newState2.items.length).toEqual(0);
    //Tear down
  });


  it('should reduce vote Up', () =>{
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const item: DestinoViaje = new DestinoViaje('Paris', 'localhost:300/url_de_prueba', 0);
    const newItem: NuevoDestinoAccion = new NuevoDestinoAccion(item);
    const voteUp: VoteUpAction = new VoteUpAction(item);
    //Action
    const newState1: DestinoViajesState = reducerDestinoViajes(prevState, newItem);
    const newState2: DestinoViajesState = reducerDestinoViajes(newState1, voteUp);
    expect(newState2.items.length).toEqual(1);
    expect(newState2.items[0].votes).toEqual(1);
    //Tear down
  });

  it('should reduce vote Down', () =>{
    //Setup
    const prevState: DestinoViajesState = initializeDestinoViajesState();
    const item: DestinoViaje = new DestinoViaje('Paris', 'localhost:300/url_de_prueba', 0);
    const newItem: NuevoDestinoAccion = new NuevoDestinoAccion(item);
    const voteDown: VoteDownAction = new VoteDownAction(item);
    //Action
    const newState1: DestinoViajesState = reducerDestinoViajes(prevState, newItem);
    const newState2: DestinoViajesState = reducerDestinoViajes(newState1, voteDown);
    expect(newState2.items.length).toEqual(1);
    expect(newState2.items[0].votes).toEqual(-1);
    //Tear down
  });


})
