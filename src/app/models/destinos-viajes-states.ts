import { DestinoViaje } from './destino-viaje';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

//Estado
export interface DestinoViajesState {
  items: DestinoViaje[];
  loading: boolean;
  favorito: DestinoViaje
}

export const initializeDestinoViajesState = function () {
  return {
    items: [],
    loading: false,
    favorito: null
  }
}



//Acciones
export enum DestinoViajesActionTypes {
  NUEVO_DESTINO = "[Destino viajes] nuevo",
  ELEGIDO_FAVORITO = "[Destino viajes] favorito",
  ELIMINAR_DESTINO = "[Destino viajes] Eliminar Destino",
  VOTE_UP = "[Destino viajes] Vote Up",
  VOTE_DOWN = "[Destino viajes] Vote Down",
  VOTE_RESET = "[Destino viajes] Vote Reset",
  INIT_MY_DATA = "[Destino viajes] Init My Data",
  CONTEO_DE_CLICKS = "[Destino viajes] Conteo de clicks",
}

export class NuevoDestinoAccion implements Action{
  type = DestinoViajesActionTypes.NUEVO_DESTINO;
  constructor(public destino: DestinoViaje){};
}

export class ElegidoFavoritoAction implements Action{
  type = DestinoViajesActionTypes.ELEGIDO_FAVORITO;
  constructor(public destino: DestinoViaje){};
}

export class EliminarDestinoAction implements Action{
  type = DestinoViajesActionTypes.ELIMINAR_DESTINO;
  constructor(public destino: DestinoViaje){};
}

export class VoteUpAction implements Action{
  type = DestinoViajesActionTypes.VOTE_UP;
  constructor(public destino: DestinoViaje){};
}

export class VoteDownAction implements Action{
  type = DestinoViajesActionTypes.VOTE_DOWN;
  constructor(public destino: DestinoViaje){};
}

export class VoteResetAction implements Action{
  type = DestinoViajesActionTypes.VOTE_RESET;
  constructor(public destino: DestinoViaje){};
}

export class InitMyDataAction implements Action{
  type = DestinoViajesActionTypes.INIT_MY_DATA;
  constructor(public destinos: string[]){};
}

export class TrackingClickAction implements Action{
  type = DestinoViajesActionTypes.CONTEO_DE_CLICKS;
  constructor(public id: string){};
}

export type DestinoViajesActions  = NuevoDestinoAccion | ElegidoFavoritoAction | EliminarDestinoAction |
VoteUpAction | VoteDownAction | InitMyDataAction | TrackingClickAction;

//Reducers
export function reducerDestinoViajes(
        state: DestinoViajesState,
        action: DestinoViajesActions,
      ): DestinoViajesState
{
  switch(action.type){
    case DestinoViajesActionTypes.INIT_MY_DATA:{
      const destinos: string[] = (action as InitMyDataAction).destinos;
      return {
        ...state,
        items: destinos.map(d => new DestinoViaje(d, ''))
      }
    }
    case DestinoViajesActionTypes.NUEVO_DESTINO:{
      return {
        ...state,
        items: [...state.items, (action as NuevoDestinoAccion).destino]
      }
    }
    case DestinoViajesActionTypes.ELIMINAR_DESTINO:{
      let eliminado: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      return {
        ...state,
        items: state.items.filter(d => d.id !== eliminado.id)
      }
    }
    case DestinoViajesActionTypes.ELEGIDO_FAVORITO:{
      let fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
      state.items.forEach(x => x.setSelected(x.id === fav.id));
      return {
        ...state,
        favorito: fav
      }
    }
    case DestinoViajesActionTypes.VOTE_UP:{
      const d: DestinoViaje = (action as VoteUpAction).destino;
      d.votesUp();
      return { ...state };
    }
    case DestinoViajesActionTypes.VOTE_DOWN:{
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.votesDown();
      return {...state };
    }
    case DestinoViajesActionTypes.VOTE_RESET:{
      const d: DestinoViaje = (action as VoteDownAction).destino;
      d.votesReset();
      return {...state };
    }
    case DestinoViajesActionTypes.CONTEO_DE_CLICKS:{
      const id: string = (action as TrackingClickAction).id;
      state.items.map(i => i.id === id ? console.log('destino : ',i) : 'sin coincidencia');
      state.items.forEach(i => {return i.id === id ? i.clicks() : i});
      console.log(state.items);
      return { ...state }
    }
  }
  return state;
}

//Effects
@Injectable()
export class DestinoViajeEffects{
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe(
    ofType(DestinoViajesActionTypes.NUEVO_DESTINO),
    map((action: NuevoDestinoAccion) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions){}
}


