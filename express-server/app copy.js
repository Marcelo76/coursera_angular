var express = require('express');
var app = express();    //Inicializa el servidor
app.use(express.json());    //Indica que utilizaremos Json
app.listen(3000, () => console.log('Servidor corriendo en el puerto 3000'));

//Configurando rutas
app.get('/url', (req, res, nest) => res.json(['Paris','Barcelona','Barranquilla','Montevideo','Santiago de Chile']));

var misDestinos = [];
app.get('/my', (req, res, nest) => res.json(misDestinos));
app.post('/my', (req, res, nest) => {
    console.log(req.body);
    misDestinos = req.body;
    res.json(misDestinos);
});
