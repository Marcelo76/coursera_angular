var express = require('express');
var cors = require('cors');
var app = express();    //Inicializa el servidor
app.use(express.json());    //Indica que utilizaremos Json
app.use(cors());
app.listen(3000, () => console.log('Servidor corriendo en el puerto 3000'));

var ciudades = ['Paris','Barcelona','Barranquilla','Montevideo','Santiago de Chile','Mexico DF', 'Nueva York'];
//Configurando rutas
app.get('/ciudades', (req, res, nest) => res.json(ciudades.filter(c => c.toLowerCase().indexOf(req.query.q.toString()) > -1)));

var misDestinos = [];
app.get('/my', (req, res, nest) => res.json(misDestinos));
app.post('/my', (req, res, nest) => {
    console.log(req.body);
    //misDestinos = req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.get("/api/translation", (req, res, next) => res.json([
    {lang: req.query.lang,  key: 'HOLA', value: 'Hola ' + req.query.lang}
]));

